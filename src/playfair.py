import numpy as NP
from ciphertexts import playfair_ciphertext as CIPHERTEXT
import textwrap
import random
from sympy.utilities.iterables import multiset_permutations
from testdata import en as ref_en
from reference import en_pairs as reference_list
from copy import deepcopy

DEBUG = 0

def make_random_square():
    square=NP.zeros([5,5],dtype=str)
    alphabet = 'ABCDEFGHIKLMNOPQRSTUVWXYZ'  # zonder j, want i=j
    length=len(alphabet)
    for i in range(5):
        for j in range(5):
            random_number=random.randint(0,length-1)
            a=alphabet[random_number]
            square[i][j]=a
            alphabet=alphabet.replace(a,'')
            length=len(alphabet)
    return square

def generate_neighbours_of_square(square):
    permutations = [(a,b) for a in list(range(25)) for b in list(range(25)) if a < b]
    def permute_square(perm):
        fl = list(square.reshape(25).copy())
        a = perm[0]
        b = perm[1]
        tmp = fl[a]
        fl[a] = fl[b]
        fl[b] = tmp
        return NP.array(fl, dtype='str').reshape(5, 5)
    res = [permute_square(x) for x in permutations]
    return res


def playfair(playfairsquare,text):
    letter_pair_list = NP.array((textwrap.wrap(text, 2)))
    if DEBUG>5:
        print(letter_pair_list)
    new_text=''
    for element in letter_pair_list:
        new_duo=''
        index1=NP.where(playfairsquare==element[0])
        index2=NP.where(playfairsquare==element[1])
        in_which_row_letter1=index1[0][0]
        in_which_column_letter1=index1[1][0]
        in_which_row_letter2=index2[0][0]
        in_which_column_letter2=index2[1][0]
        if index1[0]!=index2[0] and index1[1]!=index2[1]:
            new_duo+=playfairsquare[in_which_row_letter1][in_which_column_letter2]
            new_duo+=playfairsquare[in_which_row_letter2][in_which_column_letter1]
            new_text+=new_duo
        elif index1[0]==index2[0] and index1[1]!=index2[1]:
            if in_which_column_letter1!=0:
                new_duo+=playfairsquare[in_which_row_letter1][in_which_column_letter1-1]
            else:
                new_duo+=playfairsquare[in_which_row_letter1][4]
            if in_which_column_letter2!=0:
                new_duo+=playfairsquare[in_which_row_letter2][in_which_column_letter2-1]
            else:
                new_duo+=playfairsquare[in_which_row_letter2][4]
            new_text+=new_duo
        elif index1[0]!=index2[0] and index1[1]==index2[1]:
            if in_which_row_letter1!=0:
                new_duo+=playfairsquare[in_which_row_letter1-1][in_which_column_letter1]
            else:
                new_duo+=playfairsquare[4][in_which_column_letter1]
            if in_which_row_letter2!=0:
                new_duo+=playfairsquare[in_which_row_letter2-1][in_which_column_letter2]
            else:
                new_duo+=playfairsquare[4][in_which_column_letter2]
            new_text+=new_duo
    return new_text

def frequency_analysis_of_duos(ref_text):
    # takes awhile. So therefore the result is now saved in ref.en_pairs for future use
    #print('lengte reftext: '+ str(len(ref_text)))

    blocksize = 2
    result_list = []
    letter_pair_list = NP.array((textwrap.wrap(ref_text, blocksize)))
    unique_pairs = NP.unique(letter_pair_list)
    for pair in unique_pairs:
        occurrences = NP.count_nonzero(letter_pair_list == pair) / float(letter_pair_list.shape[0]) * 100
        result_list.append((pair, occurrences))
    result_list = NP.array(result_list, dtype=[('x', 'O'), ('y', 'f')])
    result_list.sort(order='y')
    return list(result_list[::-1])

# print(frequency_analysis_of_duos(ref_en))


def calculate_fitness(square, reference):
    transposed_text = playfair(square, CIPHERTEXT)
    frequency_list = frequency_analysis_of_duos(transposed_text)
    frequencies = [x[1] for x in frequency_list]
    letter_pairs = [x[0] for x in frequency_list]
    res_list = []
    for ref in reference:
        if ref[0] in letter_pairs:
            index = letter_pairs.index(ref[0])
            score = (ref[1] - frequencies[index])**2
        else:
            score = ref[1] ** 2
        res_list.append(score)
    letter_pairs_ref = [x[0] for x in reference]
    for freq in frequency_list:
        if freq[0] not in letter_pairs_ref:
            score = freq[1] ** 2
            res_list.append(score)
    return sum(res_list)

def choose_next_square(all_neighbours):
    score_list = [calculate_fitness(x, reference_list) for x in all_neighbours]
    index = score_list.index(min(score_list))
    return all_neighbours[index], min(score_list)


def hill_climbers(max_rounds, previous_square):
    two_ago =  previous_square
    first_text = playfair(previous_square,CIPHERTEXT)
    print(first_text)
    i = 0
    while True:
        neighbours = generate_neighbours_of_square(previous_square)
        current_square, current_score = choose_next_square(neighbours)
        current_text = playfair(current_square, CIPHERTEXT)
        print(str(i),str(current_score), current_text)
        if DEBUG>3:
            print(current_square)
        diff = current_square == two_ago
        same = diff.all()
        if i >= max_rounds or same:
            break
        else:
            two_ago = previous_square
            previous_square = current_square
            i += 1
    return current_score, current_text, current_square

def decode(num_of_initial_squares, max_iterations_in_hill_climber, start=None):
    if type(start) != NP.ndarray:
        start = make_random_square()
    xxx=[]
    for i in range(num_of_initial_squares):
        xxx.append(hill_climbers(max_iterations_in_hill_climber, start))
    print('final results')
    for x in xxx:
        print(x)

decode(3, 60)

start = NP.array([['H', 'E', 'T', 'B', 'R'],
       ['N', 'M', 'Y', 'S', 'A'],
       ['C', 'D', 'F', 'G', 'I'],
       ['U', 'L', 'W', 'X', 'Z'],
       ['K', 'V', 'O', 'P', 'Q']], dtype='<U1')

#DEBUG = 4
#print(start)
#print(playfair(start,CIPHERTEXT[24:28]))
#decode(1,60,start)
