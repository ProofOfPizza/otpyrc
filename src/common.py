import math as M

def n_choose_k(n, k):
    f = M.factorial
    return f(n) // (f(k) * f(n-k))


