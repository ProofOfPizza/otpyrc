import textwrap
import common as Common
from ciphertexts import vigenere_ciphertext as CIPHERTEXT
import reference as ref
import itertools as I
import numpy as NP
from results import a
from collections import defaultdict
import math
from sympy.utilities.iterables import multiset_permutations

CIPHER_LENGTH = 3
NUMBER_OF_RESULTS_CONSIDERED = 10
REFERENCE = ref.dutch


def do_permutations(cipherlength):
    list_of_numbers = range(1, cipherlength + 1)
    l = list(I.permutations(list_of_numbers))
    return l


def put_stop_in_text(ciphertext, cipherlength, permutation):  # doet - op plaats waar korte rijen zitten
    number_of_long_columns = len(ciphertext) % cipherlength
    number_of_short_columns = cipherlength - number_of_long_columns
    length_of_short_columns = int(len(ciphertext) / cipherlength)
    length_of_long_columns = length_of_short_columns + 1
    dict_with_distribution = dict()
    i = 0
    index = 0
    for element in range(number_of_long_columns):
        dict_with_distribution[element + 1] = 'I'
        i += 1
    for k in range(i, cipherlength):
        dict_with_distribution[k + 1] = '0'
    for element in permutation:
        for k, v in dict_with_distribution.items():
            if k == element:
                if v == 'I':
                    number_of_long_columns += 1
                    index += length_of_long_columns
                elif v == '0':
                    number_of_short_columns += 1
                    index += length_of_short_columns
                    ciphertext = ciphertext[:index] + '-' + ciphertext[index:]
                    index += 1
    # print(dict_with_distribution)
    a = [ciphertext[i:i + length_of_long_columns] for i in range(0, len(ciphertext), length_of_long_columns)]
    # print(a)
    new_list = []
    i = 0
    for element in permutation:
        new_list.append(a[element - 1])
        i += 1
    # print(new_list)
    return new_list


def make_text_of_list(list):
    length = len(list[0])
    i = 0
    new_text = ''
    while i < length:
        for element in list:
            if element[i] != '-':
                new_text += element[i]
        i += 1
    # print('nnnnnn', new_text)
    return new_text


make_text_of_list(put_stop_in_text('ABCDEFGHIJ', 3, [2, 1, 3]))


def faculty(number):
    product = 1
    for element in range(1, number + 1):
        product *= element
    return product


def make_all_texts(ciphertext, cipherlength):
    permutations = do_permutations(cipherlength)
    number_of_results = faculty(cipherlength)
    result = NP.zeros(number_of_results, dtype='O')
    index = 0
    for p in permutations:
        x1 = put_stop_in_text(ciphertext, cipherlength, p)
        x2 = make_text_of_list(x1)
        result[index] = x2
        index += 1
    return result


def find_gcd_of_2_elements(x, y):
    while (y):
        x, y = y, x % y
    return x


def find_gcd(list_with_numbers):
    x = list_with_numbers[0]
    y = list_with_numbers[1]
    gcd = find_gcd_of_2_elements(x, y)
    for i in range(2, len(list_with_numbers)):
        gcd = find_gcd_of_2_elements(gcd, list_with_numbers[i])
    return gcd


def search_g_c_d(list_of_places_of_repeated_strings):
    list_after_substraction = []
    for i in range(len(list_of_places_of_repeated_strings)):
        if i + 1 != len(list_of_places_of_repeated_strings):
            after_substraction = list_of_places_of_repeated_strings[i + 1] - list_of_places_of_repeated_strings[i]
            list_after_substraction.append(after_substraction)
    gcd = find_gcd(list_after_substraction)
    return gcd


def look_for_cipher_length(ciphertext, cipherlength):  # dieciohertext zullen we dus laten varieren van 5 tot 10
    all_texts = make_all_texts(ciphertext, cipherlength)
    # print(all_texts)
    sum_per_text = list()
    sum_per_text2 = list()
    list_of_gcd = list()
    for text in all_texts:
        # print(text)
        string_reference = text[0:2]  # de drie eerste letters van de tekst
        places_of_repeated_strings = list()  # plaatsen waar de herhaling plaatsvindt, zo kan ggd bepaald worden
        sum = 0  # hoeft niet want check je in volgende functie ;)
        for i in range(len(text)):
            if text[i:i + 2] == string_reference:
                sum = sum + 1
                places_of_repeated_strings.append(i)
        sum_per_text.append([text, sum, places_of_repeated_strings])
        if len(places_of_repeated_strings) >= 3:
            gcd = search_g_c_d(places_of_repeated_strings)
            if gcd >= 5 and gcd <= 10:
                list_of_gcd.append(gcd)
                sum_per_text2.append([text, gcd])
        elif len(places_of_repeated_strings) == 2:
            gcd = find_gcd_of_2_elements(places_of_repeated_strings[0], places_of_repeated_strings[1])
            if gcd >= 5 and gcd <= 10:
                list_of_gcd.append(gcd)
                sum_per_text2.append([text, gcd])
        elif len(places_of_repeated_strings) == 1:
            gcd = places_of_repeated_strings[0]
            if gcd >= 5 and gcd <= 10:
                list_of_gcd.append(gcd)
                sum_per_text2.append([text, gcd])
    number_of_gcd_5 = 0
    number_of_gcd_6 = 0
    number_of_gcd_7 = 0
    number_of_gcd_8 = 0
    number_of_gcd_9 = 0
    number_of_gcd_10 = 0
    for element in list_of_gcd:
        if element == 5:
            number_of_gcd_5 += 1
        if element == 6:
            number_of_gcd_6 += 1
        if element == 7:
            number_of_gcd_7 += 1
        if element == 8:
            number_of_gcd_8 += 1
        if element == 9:
            number_of_gcd_9 += 1
        if element == 10:
            number_of_gcd_10 += 1
    print('gcd_5:', number_of_gcd_5)
    print('gcd_6:', number_of_gcd_6)
    print('gcd_7:', number_of_gcd_7)
    print('gcd_8:', number_of_gcd_8)
    print('gcd_9:', number_of_gcd_9)
    print('gcd_10:', number_of_gcd_10)
    maximum = sum_per_text[0][1]

    '''
    for x in sum_per_text:
        if x[1] > maximum:
            maximum = x[1]
            the_best_text = x[0]
            indices_of_repeated_strings = x[2]
    space_between_repeated_strings = list()

    for index in range(len(indices_of_repeated_strings)):
        if index == 4:
            space_between_repeated_strings = space_between_repeated_strings
        else:
            space_between_repeated_strings.append(indices_of_repeated_strings[index + 1] - indices_of_repeated_strings[index])
    greatest_common_divisor = math.reduce(math.gcd(), list(
        list(space_between_repeated_strings) | list(space_between_repeated_strings)))
    return greatest_common_divisor
    '''
    return sum_per_text2
    # greatest_common_divisor=search_g_c_d(indices_of_repeated_strings)

    # return greatest_common_divisor


def frequency_analysis(list_with_results):
    # maakt dict met frequencies op 0 van heel alfabet
    alfabet = 'abcdefghijklmnopqrstuvwxyz'
    big_list_with_results = []
    for element in list_with_results:
        text = element[0]
        cipherlength = element[1]
        length_one_column = 0
        dict_with_results1 = dict()

        for element in alfabet:
            dict_with_results1[element] = 0

        for i in range(len(text)):
            if i % cipherlength == 0:
                dict_with_results1[text[i]] += 1
                length_one_column += 1
            i += 1

        result_list = []
        for k, v in dict_with_results1.iteritems():
            result_list.append((k, (v / float(length_one_column)) * 100))
        big_list_with_results.append([text, result_list])
    return big_list_with_results


def sort_max_to_min(frequency_list):
    for i in range(len(frequency_list) - 1):
        if frequency_list[i][1] < frequency_list[i + 1][1]:
            tmp = frequency_list[i]
            frequency_list[i] = frequency_list[i + 1]
            frequency_list[i + 1] = tmp
            sort_max_to_min(frequency_list)
    return frequency_list


def calculate_fitness(frequency_list, reference):  # Deze moeten beiden lijsten zijn met tuples!
    nieuwelijst = list()
    reference = sort_max_to_min(reference)
    frequency_list = sort_max_to_min(frequency_list)
    for i in range(len(reference)):
        nieuwelijst.append((reference[i][0], frequency_list[i][0], abs(reference[i][1] - frequency_list[i][1])))
        # nieuwelijst.append((reference[i][0], frequency_list[i][0], abs(reference[i][1] - frequency_list[i][1]) * (1+1) ))
    return nieuwelijst


def calculate_average(list_of_frequencies):
    sum = 0
    for i in range(len(list_of_frequencies)):
        sum += (list_of_frequencies[i][2]) ** 2
    average = sum / len(list_of_frequencies)
    return average


def analyse_letter_pairs(text, blocksize=1):
    result_list = []
    letter_pair_list = NP.array((textwrap.wrap(text, blocksize)))
    unique_pairs = NP.unique(letter_pair_list)
    if len(unique_pairs) != 26:
        print('WOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO', len(unique_pairs))
    for pair in unique_pairs:
        x = NP.count_nonzero(letter_pair_list == pair)
        y = len(text) / 2
        occurrences = NP.count_nonzero(letter_pair_list == pair) / float(letter_pair_list.shape[0]) * 100
        result_list.append((pair, occurrences))
    result_list = NP.array(result_list, dtype=[('x', 'O'), ('y', 'f')])
    result_list.sort(order='y')
    return result_list[::-1]


def get_column_length():
    if len(CIPHERTEXT) / CIPHER_LENGTH == int(len(CIPHERTEXT) / CIPHER_LENGTH):
        return len(CIPHERTEXT) / CIPHER_LENGTH
    else:
        return int(len(CIPHERTEXT) / CIPHER_LENGTH) + 1


def trim_text_end(text, columns):
    if len(text) % columns == 0:
        return text
    else:
        return trim_text_end(text[0:len(text) - 1], columns)


def generate_fitness_scores(all_possible_texts):
    score_columns = list()
    score_text = list()
    scores = list()
    total_list = list()
    for text in all_possible_texts:
        trimmed = trim_text_end(text, CIPHER_LENGTH)
        arr = textwrap.wrap(trimmed, 1)
        rows = len(trimmed) // CIPHER_LENGTH
        text_m = NP.array(arr).reshape([rows, CIPHER_LENGTH])
        for k in range(CIPHER_LENGTH):
            column = text_m[:, k]
            column_string = ''.join(list(column))
            frequency_list = analyse_letter_pairs(column_string, 1)
            score_columns.append(frequency_list)
            score_list = calculate_fitness(frequency_list, REFERENCE)
            score = calculate_average(score_list)
            scores.append(score)
        squares = [x ** 2 for x in scores]
        text_average = sum(squares) / len(scores)
        score_text.append(score_list)
        total_list.append((text_average, [text, score_text]))
    dtype = [('x', float), ('y', list)]
    res_m = NP.array(total_list, dtype)
    sorted = NP.sort(res_m, order='x')
    return sorted


# returns: a list of fitness_score_lists
def get_best_results(fitness_score_list, number_of_results_considered):
    return fitness_score_list[0:number_of_results_considered]


def subs(col, c):
    before = [x[0] for x in col]
    after = [x[1] for x in col]
    index = before.index(c)
    return after[index]


def substitute_text(score_matrix):
    text = score_matrix[1][0]
    columns = score_matrix[1][1]
    result = ['!'] * len(text)
    for i in range(CIPHER_LENGTH):
        indices = [x for x in range(len(text)) if x % CIPHER_LENGTH == i]
        for j in indices:
            result[j] = subs(columns[i], text[j])
    return result


def pretty_print(texts):
    for t in texts:
        for i in [x for x in range(0, len(t), 150)]:
            t = t[:i] + '\n' + t[i:]
        print(t, '\n')


def decode(saving=False):
    # alles van 'stap 1' in t plaatje
    print('cipher length:', CIPHER_LENGTH)
    print('Number of results considered:', NUMBER_OF_RESULTS_CONSIDERED)
    print('Applying it all and getting the transposed texts...')
    list_of_possible_texts = make_all_texts(CIPHERTEXT, CIPHER_LENGTH)
    if saving:
        NP.save('texts_vigenere.npy', list_of_possible_texts)
    print('calculating the fitness scores ...')
    fitness_scores_for_all_variations = generate_fitness_scores(list_of_possible_texts)
    if saving:
        NP.save('scores_vigenere.npy', fitness_scores_for_all_variations)
    # Alles van stap 2 wat dus eigenlijk al met stap 1 gefixt is :)
    best_results = get_best_results(fitness_scores_for_all_variations, NUMBER_OF_RESULTS_CONSIDERED)
    print('best_res: ', best_results)
    # Alles van stap 3 in t plaatje
    final_texts = NP.zeros(best_results.shape, dtype='O')
    for i in range(best_results.shape[0]):
        t = ''.join(substitute_text(best_results[i]))
        final_texts[i] = t
    return final_texts


def decode_from_transposed_texts(texts):
    # alles van 'stap 1' in t plaatje
    print('Decoding using previously saved results for finding texts undoing columnar transposition:', texts)
    list_of_possible_texts = NP.load(texts, allow_pickle=True)
    print('calculating the fitness scores ...')
    fitness_scores_for_all_variations = generate_fitness_scores(list_of_possible_texts)
    # Alles van stap 2 wat dus eigenlijk al met stap 1 gefixt is :)
    best_results = []
    # Alles van stap 3 in t plaatje
    final_texts = NP.zeros(best_results.shape, dtype='O')
    for i in range(best_results.shape[0]):
        t = ''.join(substitute_text(best_results[i]))
        final_texts[i] = t
    return final_texts


def decode_from_fitness_scores(scores):
    # alles van 'stap 1' in t plaatje
    print('Decoding using previously saved results calculating the fitness scores:', scores)
    fitness_scores_for_all_variations = NP.load(scores, allow_pickle=True)
    # Alles van stap 2 wat dus eigenlijk al met stap 1 gefixt is :)
    best_results = []
    # Alles van stap 3 in t plaatje
    final_texts = NP.zeros(best_results.shape, dtype='O')
    for i in range(best_results.shape[0]):
        t = ''.join(substitute_text(best_results[i]))
        final_texts[i] = t
    return final_texts


def sort(list_text_averages):
    l = len(list_text_averages)
    for i in range(0, l):
        for j in range(0, l - i - 1):
            if (list_text_averages[j][1] > list_text_averages[j + 1][1]):
                var = list_text_averages[j]
                list_text_averages[j] = list_text_averages[j + 1]
                list_text_averages[j + 1] = var
    return list_text_averages

def get_best_results(a):
    b = frequency_analysis(a)
    list_of_averages = []
    for element in b:
        c = calculate_fitness(element[1], REFERENCE)
        d = calculate_average(c)
        list_of_averages.append([element[0], d])
    sort(list_of_averages)
    best_results = list_of_averages[0:NUMBER_OF_RESULTS_CONSIDERED]
    return best_results

#print(get_best_results(a))



# decoded = decode(True)
# pretty_print(decode())
# print(decode(True))
# print(decode_from_fitness_scores('scores_7-7.npy'))
# print(decode_from_fitness_scores('scores-9-9.npy'))
# print(decode_from_transposed_texts('texts_4-7.npy'))