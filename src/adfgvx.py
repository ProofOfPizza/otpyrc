import textwrap
import common as Common
from ciphertexts import adfgvx_ciphertext as CIPHERTEXT
import reference as ref
import itertools as I
import numpy as NP
from sympy.utilities.iterables import multiset_permutations
import testdata

MIN_CIPHER_LENGTH = 7
MAX_CIPHER_LENGTH = 7
NUMBER_OF_RESULTS_CONSIDERED = 10
TOLERANCE = 2
LANGUAGE_LIST = [ref.french] #, ref.french2, ref.french3]
print(len(CIPHERTEXT))


def do_permutations(cipherlength):
    list_of_numbers=range(1,cipherlength+1)
    l=list(I.permutations(list_of_numbers))
    return l

def put_stop_in_text(ciphertext, cipherlength, permutation):  # doet - op plaats waar korte rijen zitten
    number_of_long_columns = len(ciphertext) % cipherlength
    number_of_short_columns = cipherlength - number_of_long_columns
    length_of_short_columns = int(len(ciphertext) / cipherlength)
    length_of_long_columns = length_of_short_columns + 1
    dict_with_distribution = dict()
    i = 0
    index = 0
    for element in range(number_of_long_columns):
        dict_with_distribution[element+1]='I'
        i+=1
    for k in range(i,cipherlength):
        dict_with_distribution[k+1]='0'
    for element in permutation:
        for k,v in dict_with_distribution.items():
            if k==element:
                if v == 'I':
                    number_of_long_columns += 1
                    index += length_of_long_columns
                elif v == '0':
                    number_of_short_columns += 1
                    index += length_of_short_columns
                    ciphertext = ciphertext[:index] + '-' + ciphertext[index:]
                    index += 1
    # print(dict_with_distribution)
    a = [ciphertext[i:i + length_of_long_columns] for i in range(0, len(ciphertext), length_of_long_columns)]
    new_list = []
    i = 0
    for element in permutation:
        new_list.append(a[element - 1])
        i += 1
    return new_list


a=put_stop_in_text('ABCDEFGHIJ',3,[2,1,3])
print(a)


def make_text_of_list(list):
    length=len(list[0])
    i=0
    new_text=''
    while i < length:
        for element in list:
            if element[i] != '-':
                new_text+=element[i]
        i+=1
    return new_text

print(make_text_of_list(a))
def faculty(number):
    product=1
    for element in range(1,number+1):
        product*=element
    return product



def make_all_texts(ciphertext,cipherlength):
    permutations=do_permutations(cipherlength)
    number_of_results=faculty(cipherlength)
    result=NP.zeros(number_of_results,dtype='O')
    index=0
    for p in permutations:
        x1=put_stop_in_text(ciphertext,cipherlength,p)
        x2=make_text_of_list(x1)
        result[index]=x2
        index+=1
    return result


def sort_max_to_min(frequency_list):
    for i in range(len(frequency_list) - 1):
        if frequency_list[i][1] < frequency_list[i+1][1]:
            tmp = frequency_list[i]
            frequency_list[i] = frequency_list[i+1]
            frequency_list[i+1] = tmp
            sort_max_to_min(frequency_list)
    return frequency_list

def calculate_fitness(frequency_list, reference):  # Deze moeten beiden lijsten zijn met tuples!
    nieuwelijst = list()
    reference = sort_max_to_min(reference)
    frequency_list = frequency_list
    for i in range(len(reference)):
        nieuwelijst.append((reference[i][0], frequency_list[i][0], abs(reference[i][1] - frequency_list[i][1])))
        #nieuwelijst.append((reference[i][0], frequency_list[i][0], abs(reference[i][1] - frequency_list[i][1]) * (1+1) ))
    return nieuwelijst

def calculate_average(list_of_frequencies):
    sum = 0
    for i in range(len(list_of_frequencies)):
        sum += (list_of_frequencies[i][2])  ** 2
    average = sum / len(list_of_frequencies)
    return average

def check_the_numbers(list_of_frequencies):
    sum=0
    for element in list_of_frequencies:
        sum+=1
    if 26 <= sum < 36:
        print (sum)
        return True
    elif sum==36:
        return False


def generate_fitness_scores(variation_texts):
    number_of_texts = variation_texts.shape[0]
    dtype = [('x', float), ('y', list)]
    fitness_scores_for_all_variations = NP.zeros([number_of_texts], dtype=dtype)
    delete_index_list = []
    for i in range(number_of_texts):
        if i % 1000 == 0:
            print('calculating fitness scores ', i, '/', number_of_texts)
        frequency_list = analyse_letter_pairs(variation_texts[i])
        this_score = 0
        this_score_list = []
        this_text = variation_texts[i]
        if not check_the_numbers(frequency_list):
            delete_index_list.append(i)
            continue
        print(variation_texts[i])
        for language in LANGUAGE_LIST:
            fitness_score_list = calculate_fitness(frequency_list, sort_max_to_min(language))
            fitness_score = calculate_average(fitness_score_list)
            if this_score == 0 or fitness_score < this_score:
                this_score = fitness_score
                this_score_list = fitness_score_list

        # wordt uiteindelijk iets van de vorm [(score,[fitness_score_list]),(score,[fitness_score_list]), etc ]
        fitness_scores_for_all_variations[i] = this_score, [this_score_list, this_text]
    #print(fitness_scores_for_all_variations.shape)
    fitness_scores_for_all_variations = NP.delete(fitness_scores_for_all_variations, delete_index_list)
    sorted_fitness_scores_for_all_variations = NP.sort(fitness_scores_for_all_variations, order='x')
    print('sorted list of all variations',sorted_fitness_scores_for_all_variations)
    return sorted_fitness_scores_for_all_variations


# returns: a list of fitness_score_lists
def get_best_results(fitness_score_list, number_of_results_considered):
    best_scores_and_score_lists = fitness_score_list[0:number_of_results_considered]
    matrix_best_lists = NP.array([x[1] for x in best_scores_and_score_lists])
    return matrix_best_lists


def analyse_letter_pairs(text, blocksize=2):
    result_list = []
    letter_pair_list = NP.array((textwrap.wrap(text, blocksize)))
    unique_pairs = NP.unique(letter_pair_list)
    for pair in unique_pairs:
        x = NP.count_nonzero(letter_pair_list == pair)
        y = len(text)/2
        occurrences = NP.count_nonzero(letter_pair_list == pair)  / float(letter_pair_list.shape[0]) * 100
        result_list.append((pair, occurrences))
    result_list = NP.array(result_list, dtype=[('x','O'), ('y','f')])
    result_list.sort(order='y')
    return result_list[::-1]


def substitute_text(text, score_matrix, tolerance):
    print('=====> ', type(score_matrix))
    filtered_set = NP.array([x for x in score_matrix if float(x[2]) < tolerance])
    letter_pair_matrix = NP.array(textwrap.wrap(text, 2))
    for pair in filtered_set:
        replace = NP.where(letter_pair_matrix == pair[1])
        NP.put(letter_pair_matrix, replace, pair[0])
    res = ''.join(letter_pair_matrix)
    return res

def pretty_print(texts):
    for t in texts:
        for i in [x for x in range(0, len(t), 150)]:
            t = t[:i] + '\n' + t[i:]
        print(t, '\n')

def decode(saving=False):
    # alles van 'stap 1' in t plaatje
    print('min cipher:', MIN_CIPHER_LENGTH)
    print('max cipher:', MAX_CIPHER_LENGTH)
    print('TOLERANCE:', TOLERANCE)
    print('Number of results considered:', NUMBER_OF_RESULTS_CONSIDERED)
    print('Applying it all and getting the transposed texts...')
    #list_of_possible_texts = generate_the_list_of_possible_texts_undoing_columnar_transposition(CIPHERTEXT)
    list_of_possible_texts = make_all_texts(CIPHERTEXT, MIN_CIPHER_LENGTH)
    if saving:
        NP.save('texts.npy', list_of_possible_texts)

    print('calculating the fitness scores ...')
    fitness_scores_for_all_variations = generate_fitness_scores(list_of_possible_texts)
    if saving:
        NP.save('scores.npy', fitness_scores_for_all_variations)

    # Alles van stap 2 wat dus eigenlijk al met stap 1 gefixt is :)
    best_results = get_best_results(fitness_scores_for_all_variations, NUMBER_OF_RESULTS_CONSIDERED)
    print('best_res: ', best_results)
    # Alles van stap 3 in t plaatje
    final_texts = NP.zeros(best_results.shape[0], dtype='O')
    for i in range(best_results.shape[0]):
        final_texts[i] = substitute_text(best_results[i][1], best_results[i][0], TOLERANCE)
    return final_texts

def decode_from_transposed_texts(texts):
    # alles van 'stap 1' in t plaatje
    print('Decoding using previously saved results for finding texts undoing columnar transposition:', texts)
    list_of_possible_texts = NP.load(texts, allow_pickle=True)
    print('calculating the fitness scores ...')
    fitness_scores_for_all_variations = generate_fitness_scores(list_of_possible_texts)
    #print(fitness_scores_for_all_variations[0:5])

    # Alles van stap 2 wat dus eigenlijk al met stap 1 gefixt is :)
    best_results = get_best_results(fitness_scores_for_all_variations, NUMBER_OF_RESULTS_CONSIDERED)
    print('best_res: ', best_results)
    # Alles van stap 3 in t plaatje
    final_texts = NP.zeros(best_results.shape[0], dtype='O')
    for i in range(best_results.shape[0]):
        final_texts[i] = substitute_text(best_results[i][1], best_results[i][0], TOLERANCE)
    return final_texts

def decode_from_fitness_scores(scores):
    # alles van 'stap 1' in t plaatje
    print('Decoding using previously saved results calculating the fitness scores:', scores)
    fitness_scores_for_all_variations = NP.load(scores, allow_pickle=True)

    # Alles van stap 2 wat dus eigenlijk al met stap 1 gefixt is :)
    best_results = get_best_results(fitness_scores_for_all_variations, NUMBER_OF_RESULTS_CONSIDERED)
    print('best_res: ', best_results)
    # Alles van stap 3 in t plaatje
    final_texts = NP.zeros(best_results.shape[0], dtype='O')
    for i in range(best_results.shape[0]):
        final_texts[i] = substitute_text(best_results[i][1], best_results[i][0], TOLERANCE)
    return final_texts

def replace_letter(string, letter1,letter2):
    new_string=''
    for element in string:
        if element==letter1:
            new_string+=letter2
        elif element==letter2:
            new_string+=letter1
        else:
            new_string+=element
    return new_string

pretty_print(decode(True))
#pretty_print(decode_from_fitness_scores('scores.npy'))
#pretty_print(decode_from_fitness_scores('scores.npy'))
#pretty_print(decode_from_transposed_texts('texts.npy'))

