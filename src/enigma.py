rotor_0 = 'AJDKSIRUXBLHWTMCQGZNPYFVOE'
rotor_1 = 'EKMFLGDQVZNTOWYHXUSPAIBRCJ'
rotor_2 = 'BDFHJLCPRTXVZNYEIWGAKMUSQO'
rotor_3 = 'THEQUICKBROWNFXJMPSVLAZYDG'
rotor_4 = 'XANTIPESOKRWUDVBCFGHJLMQYZ'
reflector = 'YRUHQSLDPXNGOKMIEBFZCWVJAT'
enigma_ciphertext = 'AYCHNVHYATLIRHVIYYPFCRWOSLNOOPHKKCRISEEHOSTZASIUYYAAVYJVQHUKMQAWKNTWKUFPHBBIAFPOVCDCMVDWHKJSZZXNMEPKCMLTPWPKCSYQOQTTBDUBLZWWQHVLOHJJSTORLPQWWMXHLHZFHOHJFJPVUISKUFCIYTPFLJWMGFUCWFRPWBWBFCVIBMCXDKSUOISJDVJIEZDJIXJHAOUKQYXPQYBRRGYLCUVIHEVMUUTJJVZIELNRZWKACHKYXVCNNGMGWVORDXVZZEOAGEHVPFMAXTJVHLGMQSGWQYAYQWONAHAECYZANMJDBBMHMZGDTKEFJRWGDKSJDCYZRHQFPPHQFWQCLBPESZPBAXFACWXQDLBLOQJTPNCPKGECMTRSHZBNZRUFKTOGETEGTIQQLABJGYJFZQPQSFJGGWBVFKHIGWBICWGMYYDUHETEVOYTMBLOALPAUJWTABUCHHOIMXRASLUIUUGTFIJKBUPLOTOKYJWHLLXBYPPXHZAHFRWZPMWNMYVEKTKTQSSHITDXYEZJSOUSGPENKOLXYMKXMXKFOTZNVTLZMUURCVVMUNSOVXRWRCAOWFTNPMHPDLIPOZAXIDRKJLPWMIVDZJFXTWQCYUJVKOWJSOCFLECYNABYEUPXYPVCEOARYWKWMFJHRBIVRZNNKLEKHQDZFBGEQNLHAEUAREYDPQRFJCQVCXFFAGRGIXZZVEYKGOAJSOYBCSADARNIYTFCEHEKMNCSZZPZHSNGVHCBNIPJFQFSCHIKFJSIXNJLQHCPJUCTMLWTDVBAWUXTQHVMYIWBFDDPPQWGHNUBFAZKNUCPTLEYVBUTLZBTJAORXAUZHXDATXZBBAEZDQMELHFBFQJCSVNJQJHMUOZMMTLIEAZQQYIFXADWMYCGEJDIQFZYESLTMAPKHKZNTSUXPRBHVLTDDSDPYOVWAPXTOUJTCYYDYCSDKNUIWIERRVAEHBYTFAIWQNXVVOWVTAGQHIPMEUKDASZREITNKXNYNMOUFZDJOPKIHVVIQGRVSPJHLEOSKWTJKSYPUTXUZPJGGIGBOFYQOJKTUPEGPTOKDMDXEGEIHTCESHHTBDGYGCKOUEJNTQLJGUVPOYHYMOHVDVQRGVOOQNVYLYDHCLKQMIRDIRKTPZABVQPXRPJRPXLAANPMPHNITLYSSCNXJZHMUUNOCRARQUUPJNGQLVTJASORQOKEQVJCSAPEGGVGJCHRHUOZAMGTLLAKXQDRSLLLNINIRZKDXJFZTIBVLRUGMZERYISGXFAULECFGFFKLFPNBJUJANRDZOZSFZSGEGEOIULQPATIYGPSBTJGHIGERXHUZTWGKHNXKSZYRFWZRDRBXMBVUZBMDHOJXYFXPKWTPSUVAZUQRSAWLKNKKQWYNCNCPSBFFAEALTVTCJTOSEESMZVSJXIEVKSYMPLTMVOECGIJEUPWIHNNDBCYHXEOMNIXEFGVSFLMPPTADHZELNQAQORZVPQKBNELHPROIGSJWAXUWHHNLRLEYIFBOCDLBRHLGQFGNRFFRBYEQEQQMXABUKFOZAMAHQVZJAQXXYBYCIZSSBQTCMZWONICOZKUFVCNVFDPAWCQZJYPVZCOJMODJSQPZLEZIGFVHFFVSBLFPQLLHYTOHDGMZEGCUQILGLHAWBBVYSADRBMDYMRQXAVNLCTEXOKOMACQOEJNTMMZTMNHZKDVJZKZWSENZNXMWCZWFRGTOGYZVIWDXHYQMPWSQKGLJUGAWVFQWZYXZVFIVMQQBEIYPRZRQZJCQRFAIWHTQYHWZWTFBNUMYBIOQJVXUUIWDYCUFDOSVMSVRORONCYCUGKTWVQLYTSTGASYFEGSEPNVHYNVXLQKKIAPHXRTLEQFDLIZIEZIAFHXGYXLPAHHRBSGTJPFEYCYUGUXLSMPFHEZLVWMWCFGCJFZJGZPWRMWXDWKHJOXVBWPAVAWHBDIHVCDTXGJGFPNWAKSMGTTVHHGWUFTYEAEVBNKKTNKAICENYLEVIHTZUPKIMMQOVMLSGMXNNQQOLLVTZITHZTGYYWECEWQPQFUFHBFXKTHNGIJAYPDECDYLLHHZDWOKQHFVCEXSFTMSUTWCKDDDKJRZMWKSCXHPKKPAKQWTCCHQWFOJPJPVWNGIDERQEDUDCAWOKLVJTIGCQVIRZUVQIPHRUGNLUXCOFVDEPGIPAHVPBWYBHGOQMHLLVIDOREQNYXBAXGAFMSZBDZVDINQHEQVOQYVORWXEGAXLZBMQQXOIUNGHNXADYRKRVFJOKUJMTOWMLHHNSOTTIHPQFSRAWYWMOXMHYSDBDAWOZQRSD'

ROTORS= [rotor_0,rotor_1,rotor_2,rotor_3,rotor_4]
crib = 'DEOPGAVEVOORENIGMA'

import string
from collections import defaultdict


enigma_ciphertext = 'AYCHNVHYATLIRHVIYYPFCRWOSLNOOPHKKCRISEEHOSTZASIUYYAAVYJVQHUKMQAWKNTWKUFPHBBIAFPOVCDCMVDWHKJSZZXNMEPKCMLTPWPKCSYQOQTTBDUBLZWWQHVLOHJJSTORLPQWWMXHLHZFHOHJFJPVUISKUFCIYTPFLJWMGFUCWFRPWBWBFCVIBMCXDKSUOISJDVJIEZDJIXJHAOUKQYXPQYBRRGYLCUVIHEVMUUTJJVZIELNRZWKACHKYXVCNNGMGWVORDXVZZEOAGEHVPFMAXTJVHLGMQSGWQYAYQWONAHAECYZANMJDBBMHMZGDTKEFJRWGDKSJDCYZRHQFPPHQFWQCLBPESZPBAXFACWXQDLBLOQJTPNCPKGECMTRSHZBNZRUFKTOGETEGTIQQLABJGYJFZQPQSFJGGWBVFKHIGWBICWGMYYDUHETEVOYTMBLOALPAUJWTABUCHHOIMXRASLUIUUGTFIJKBUPLOTOKYJWHLLXBYPPXHZAHFRWZPMWNMYVEKTKTQSSHITDXYEZJSOUSGPENKOLXYMKXMXKFOTZNVTLZMUURCVVMUNSOVXRWRCAOWFTNPMHPDLIPOZAXIDRKJLPWMIVDZJFXTWQCYUJVKOWJSOCFLECYNABYEUPXYPVCEOARYWKWMFJHRBIVRZNNKLEKHQDZFBGEQNLHAEUAREYDPQRFJCQVCXFFAGRGIXZZVEYKGOAJSOYBCSADARNIYTFCEHEKMNCSZZPZHSNGVHCBNIPJFQFSCHIKFJSIXNJLQHCPJUCTMLWTDVBAWUXTQHVMYIWBFDDPPQWGHNUBFAZKNUCPTLEYVBUTLZBTJAORXAUZHXDATXZBBAEZDQMELHFBFQJCSVNJQJHMUOZMMTLIEAZQQYIFXADWMYCGEJDIQFZYESLTMAPKHKZNTSUXPRBHVLTDDSDPYOVWAPXTOUJTCYYDYCSDKNUIWIERRVAEHBYTFAIWQNXVVOWVTAGQHIPMEUKDASZREITNKXNYNMOUFZDJOPKIHVVIQGRVSPJHLEOSKWTJKSYPUTXUZPJGGIGBOFYQOJKTUPEGPTOKDMDXEGEIHTCESHHTBDGYGCKOUEJNTQLJGUVPOYHYMOHVDVQRGVOOQNVYLYDHCLKQMIRDIRKTPZABVQPXRPJRPXLAANPMPHNITLYSSCNXJZHMUUNOCRARQUUPJNGQLVTJASORQOKEQVJCSAPEGGVGJCHRHUOZAMGTLLAKXQDRSLLLNINIRZKDXJFZTIBVLRUGMZERYISGXFAULECFGFFKLFPNBJUJANRDZOZSFZSGEGEOIULQPATIYGPSBTJGHIGERXHUZTWGKHNXKSZYRFWZRDRBXMBVUZBMDHOJXYFXPKWTPSUVAZUQRSAWLKNKKQWYNCNCPSBFFAEALTVTCJTOSEESMZVSJXIEVKSYMPLTMVOECGIJEUPWIHNNDBCYHXEOMNIXEFGVSFLMPPTADHZELNQAQORZVPQKBNELHPROIGSJWAXUWHHNLRLEYIFBOCDLBRHLGQFGNRFFRBYEQEQQMXABUKFOZAMAHQVZJAQXXYBYCIZSSBQTCMZWONICOZKUFVCNVFDPAWCQZJYPVZCOJMODJSQPZLEZIGFVHFFVSBLFPQLLHYTOHDGMZEGCUQILGLHAWBBVYSADRBMDYMRQXAVNLCTEXOKOMACQOEJNTMMZTMNHZKDVJZKZWSENZNXMWCZWFRGTOGYZVIWDXHYQMPWSQKGLJUGAWVFQWZYXZVFIVMQQBEIYPRZRQZJCQRFAIWHTQYHWZWTFBNUMYBIOQJVXUUIWDYCUFDOSVMSVRORONCYCUGKTWVQLYTSTGASYFEGSEPNVHYNVXLQKKIAPHXRTLEQFDLIZIEZIAFHXGYXLPAHHRBSGTJPFEYCYUGUXLSMPFHEZLVWMWCFGCJFZJGZPWRMWXDWKHJOXVBWPAVAWHBDIHVCDTXGJGFPNWAKSMGTTVHHGWUFTYEAEVBNKKTNKAICENYLEVIHTZUPKIMMQOVMLSGMXNNQQOLLVTZITHZTGYYWECEWQPQFUFHBFXKTHNGIJAYPDECDYLLHHZDWOKQHFVCEXSFTMSUTWCKDDDKJRZMWKSCXHPKKPAKQWTCCHQWFOJPJPVWNGIDERQEDUDCAWOKLVJTIGCQVIRZUVQIPHRUGNLUXCOFVDEPGIPAHVPBWYBHGOQMHLLVIDOREQNYXBAXGAFMSZBDZVDINQHEQVOQYVORWXEGAXLZBMQQXOIUNGHNXADYRKRVFJOKUJMTOWMLHHNSOTTIHPQFSRAWYWMOXMHYSDBDAWOZQRSD'


def generate_possible_k():
    list_possible_k = []
    element_k = ''
    for i in string.ascii_lowercase:
        element_k += i
        for j in string.ascii_lowercase:
            element_k += j
            for l in string.ascii_lowercase:
                element_k += l
                list_possible_k.append(element_k)
                element_k = element_k[:2]
            element_k = element_k[:1]
        element_k = ''
    list_possible_k[:] = [x.upper() for x in list_possible_k]
    return list_possible_k

possible_k=generate_possible_k()


def make_a_graph(crib, ciphertext):
    part_we_need = ciphertext[0:len(crib)]
    graph = defaultdict(list)
    i = 0
    for number in range(len(crib)):
        duo = crib[number] + part_we_need[i]
        if len(graph.keys()) == 0:
            graph[duo].append(i)
            i += 1
        else:
            if duo in graph:
                graph[duo].append(i)
            elif duo[::-1] in graph:
                graph[duo[::-1]].append(i)
            else:
                graph[duo].append(i)
            i += 1
    return graph
print(make_a_graph(crib,enigma_ciphertext))

g = make_a_graph(crib, enigma_ciphertext)
# print(g)
print(possible_k.index('NDU'))

possible_loops = defaultdict(list)
possible_loops['VAYERIV'].append('5,17,1,12,11,14')
possible_loops['VAYERIV'].append('5,17,7,12,11,14')
possible_loops['VAYERIV'].append('8,17,1,12,11,14')
possible_loops['VAYERIV'].append('8,17,7,12,11,14')
possible_loops['VIREYAV'].append('14,11,12,1,17,5')
possible_loops['VIREYAV'].append('14,11,12,7,17,5')
possible_loops['VIREYAV'].append('14,11,12,1,17,8')
possible_loops['VIREYAV'].append('14,11,12,7,17,8')
possible_loops['VAYERIGNHV'].append('5,17,1,12,11,15,4,13,6')
possible_loops['VAYERIGNHV'].append('5,17,7,12,11,15,4,13,6')
possible_loops['VAYERIGNHV'].append('8,17,1,12,11,15,4,13,6')
possible_loops['VAYERIGNHV'].append('8,17,7,12,11,15,4,13,6')
possible_loops['VHNGIREYAV'].append('6,13,4,15,11,12,1,17,5')
possible_loops['VHNGIREYAV'].append('6,13,4,15,11,12,7,17,5')
possible_loops['VHNGIREYAV'].append('6,13,4,15,11,12,1,17,8')
possible_loops['VHNGIREYAV'].append('6,13,4,15,11,12,7,17,8')
possible_loops['VHNGIV'].append('6,13,4,15,14')
possible_loops['VIGNHV'].append('14,15,4,13,6')

'''
possible_loops3 = defaultdict(list)
possible_loops3['AVIREYA'].append('5,14,11,12,7,17')
possible_loops3['AVIREYA'].append('8,14,11,12,7,17')
possible_loops3['AVIREYA'].append('5,14,11,12,1,17')
possible_loops3['AVIREYA'].append('8,14,11,12,1,17')
possible_loops3['AYERIVA'].append('17,7,12,11,14,5')
possible_loops3['AYERIVA'].append('17,7,12,11,14,8')
possible_loops3['AYERIVA'].append('17,1,12,11,14,5')
possible_loops3['AYERIVA'].append('17,1,12,11,14,8')
possible_loops3['AVHNGIREYA'].append('5,6,13,4,15,11,12,1,17')
possible_loops3['AVHNGIREYA'].append('8,6,13,4,15,11,12,1,17')
possible_loops3['AVHNGIREYA'].append('5,6,13,4,15,11,12,7,17')
possible_loops3['AVHNGIREYA'].append('8,6,13,4,15,11,12,7,17')
possible_loops3['AYERIGNHVA'].append('17,1,12,11,15,4,13,6,5')
possible_loops3['AYERIGNHVA'].append('17,7,12,11,15,4,13,6,5')
possible_loops3['AYERIGNHVA'].append('17,1,12,11,15,4,13,6,8')
possible_loops3['AYERIGNHVA'].append('17,7,12,11,15,4,13,6,8')

'''
def what_happens_in_the_rotors(slowr, middler, fastr, position, letter, reflector):
    alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    position_letter_in_alphabet = alphabet.find(letter)
    start_position_slowr = alphabet.find(position[0])
    start_position_middler = alphabet.find(position[1])
    start_position_fastr = alphabet.find(position[2])

    new_letter= alphabet[(position_letter_in_alphabet + start_position_fastr)%26]
    position_letter_in_alphabet = alphabet.find(new_letter)
    new_letter = fastr[position_letter_in_alphabet]
    position_letter_in_alphabet = alphabet.find(new_letter)
    new_letter=alphabet[(position_letter_in_alphabet-start_position_fastr)%26]
    position_letter_in_alphabet = alphabet.find(new_letter)


    new_letter= alphabet[(position_letter_in_alphabet + start_position_middler)%26]
    position_letter_in_alphabet = alphabet.find(new_letter)
    new_letter = middler[position_letter_in_alphabet]
    position_letter_in_alphabet = alphabet.find(new_letter)
    new_letter=alphabet[(position_letter_in_alphabet-start_position_middler)%26]
    position_letter_in_alphabet = alphabet.find(new_letter)

    new_letter= alphabet[(position_letter_in_alphabet + start_position_slowr)%26]
    position_letter_in_alphabet = alphabet.find(new_letter)
    new_letter = slowr[position_letter_in_alphabet]
    position_letter_in_alphabet = alphabet.find(new_letter)
    new_letter=alphabet[(position_letter_in_alphabet-start_position_slowr)%26]
    position_letter_in_alphabet = alphabet.find(new_letter)

    new_letter=reflector[position_letter_in_alphabet]
    position_letter_in_alphabet = alphabet.find(new_letter)

    new_letter= alphabet[(position_letter_in_alphabet + start_position_slowr)%26]
    position_letter_in_rotor = slowr.find(new_letter)
    new_letter = alphabet[position_letter_in_rotor]
    position_letter_in_alphabet = alphabet.find(new_letter)
    new_letter=alphabet[(position_letter_in_alphabet-start_position_slowr)%26]
    position_letter_in_alphabet = alphabet.find(new_letter)

    new_letter= alphabet[(position_letter_in_alphabet + start_position_middler)%26]
    position_letter_in_rotor = middler.find(new_letter)
    new_letter = alphabet[position_letter_in_rotor]
    position_letter_in_alphabet = alphabet.find(new_letter)
    new_letter= alphabet[(position_letter_in_alphabet - start_position_middler)%26]
    position_letter_in_alphabet = alphabet.find(new_letter)

    new_letter=alphabet[(position_letter_in_alphabet+start_position_fastr)%26]
    position_letter_in_rotor = fastr.find(new_letter)
    new_letter = alphabet[position_letter_in_rotor]
    position_letter_in_alphabet = alphabet.find(new_letter)
    new_letter=alphabet[(position_letter_in_alphabet-start_position_fastr)%26]

    return new_letter


def test_plugboard_ones(slowr, middler, fastr, loop,letters_loop,reflector, position_rotor):
    alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    loop = loop.split(',')
    loop[:] = [int(x) for x in loop]
    index_of_position_in_list = possible_k.index(position_rotor)
    list_with_results = []
    for letter in alphabet:
        new_position_rotor = possible_k[(index_of_position_in_list + loop[0])%17576]
        result = what_happens_in_the_rotors(slowr, middler, fastr, new_position_rotor, letter, reflector)
        list_with_results.append(result)
    return list_with_results



def test_plugboard_ones_for_few_letters(slowr,middler,fastr,loop,letters_loop,reflector,position_rotor,few_letters):
    loop = loop.split(',')
    loop[:] = [int(x) for x in loop]
    index_of_position_in_list = possible_k.index(position_rotor)
    list_with_results=[]
    for letter in few_letters:
        new_position_rotor = possible_k[(index_of_position_in_list + loop[0]) % 17576]
        result = what_happens_in_the_rotors(slowr, middler, fastr, new_position_rotor, letter, reflector)
        list_with_results.append(result)
    return list_with_results



def test_plugboard_for_the_others(slowr, middler, fastr, loop, letters_loop, reflector, position_rotor,
                                  list_with_results):
    loop = loop.split(',')
    loop[:] = [int(x) for x in loop]
    index_of_position_in_list_of_k = possible_k.index(position_rotor)
    new_list_with_results = []
    for j in range(1, len(loop)):
        new_position_rotor = possible_k[(index_of_position_in_list_of_k + loop[j]) % 17576]
        for element in list_with_results:
            result = what_happens_in_the_rotors(slowr, middler, fastr, new_position_rotor, element, reflector)
            #print("Bij S", loop[j], "(", element, ') is P(', letters_loop[j+1], ') =', result)
            new_list_with_results.append(result)
        list_with_results = new_list_with_results
        if j != len(loop) - 1:
            new_list_with_results = []
    return new_list_with_results


def filter_the_options(list_with_possible_plugboard_setting, letters_loop, position_rotor):
    alphabet = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U',
                'V', 'W', 'X', 'Y', 'Z']
    i = 0
    resultlist=defaultdict(list)
    list_with_results = []
    for element in list_with_possible_plugboard_setting:
        if element == alphabet[i]:
            list_with_results.append(element)
            #print('P(', letters_loop[0], ') kan dus gelijk zijn aan', element, 'bij', position_rotor)
        i += 1
    resultlist[position_rotor] = list_with_results
    return resultlist



def compare_two_lists_and_make_one(a,b):
    list_with_same_elements=[]
    for i in range(len(a)):
        if a[i]==b[i]:
            list_with_same_elements.append(a[i])
        else:
            list_with_same_elements.append('')
    return list_with_same_elements

def compare_two_lists(a,b):
    list_with_same_elements=compare_two_lists_and_make_one(a,b)
    sum=0
    for element in list_with_same_elements:
        if element == '':
            sum+=1
    if sum==len(list_with_same_elements):
        return False
    else:
        return True


def semi_finals(slowr,middler,fastr,reflector,position_rotor,resultlist,number_of_loops):
    rotors=[]
    for k,v in resultlist.items():
        x=v
        bool = True
        i=0
        if len(v)!=0:
            for k1,v1 in possible_loops2.items():
                if bool:
                    for element in v1:
                        bool = compare_two_lists(x, v)
                        if bool:
                            x1=test_plugboard_ones_for_few_letters(slowr,middler,fastr,element,k1,reflector,position_rotor,v)
                            x=test_plugboard_for_the_others(slowr,middler,fastr,element,k1,reflector,position_rotor,x1)
                            i+=1
                        else:
                    #print("rotorstand", k, 'klopt niet')
                            break
                else:
                    break
        if i==number_of_loops:
            list1=compare_two_lists_and_make_one(x,v)
        #print('rotorstand',k,'klopt')
            rotors.append(k)
            print(list1)
    return rotors


def finals(slowr,middler,fastr,reflector):
    rotors=[]
    for k in possible_k:
        beginning_letterloop=list(possible_loops2.keys())[0]
        beginning_loop=list(possible_loops2.values())[0][0]
        first_step=test_plugboard_ones(slowr,middler,fastr,beginning_loop,beginning_letterloop,reflector,k)
        second_step=test_plugboard_for_the_others(slowr,middler,fastr,beginning_loop,beginning_letterloop,reflector,k,first_step)
        third_step=filter_the_options(second_step,beginning_letterloop,k)
        fourth_step=semi_finals(slowr,middler,fastr,reflector,k,third_step,18)
        if len(fourth_step)!=0:
            rotors.append(fourth_step)

    return rotors


def test_for_every_rotor_trio():
    list_rotors=[rotor_0,rotor_1,rotor_2,rotor_3,rotor_4]
    list_with_possible_rotor_settings=[]
    for a in list_rotors:
        for b in list_rotors:
            if b!=a:
                for c in list_rotors:
                    if c!=b and c!=a:
                        f=finals(a,b,c,reflector)
                        if len(f)==0:
                            print('rotors',a,b,c,'zijn niet de rotors')
                        else:
                            print('resultaat bij rotors', a,b,c,'is',f)
                        for i in f:
                            list_with_possible_rotor_settings.append(i)
    return list_with_possible_rotor_settings


def making_the_plugboard(slowr,middler,fastr,reflector,k,letter,edge):
    position_in_list=possible_k.index(k)
    new_position=possible_k[(position_in_list+edge)%17576]
    new_letter=what_happens_in_the_rotors(slowr,middler,fastr,new_position,letter,reflector)
    return new_letter

def enigma_on_text(slowr,middler,fastr,plugboard,reflector,text,k):
    alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    index=0
    index_of_k_in_possible_k=possible_k.index(k)
    new_text=''
    for letter in text:
        new_k=possible_k[index_of_k_in_possible_k+index]
        position_letter_in_alphabet=alphabet.find(letter)
        new_letter=plugboard[position_letter_in_alphabet]
        new_letter=what_happens_in_the_rotors(slowr,middler,fastr,new_k,new_letter,reflector)
        position_letter_in_alphabet=alphabet.find(new_letter)
        new_letter=plugboard[position_letter_in_alphabet]
        new_text+=new_letter
        index+=1
    return new_text

print(making_the_plugboard(rotor_1,rotor_4,rotor_3,reflector,'NDU','U',5))
print(making_the_plugboard(rotor_1,rotor_2,rotor_4,reflector,'SMA','U',14))

print(enigma_ciphertext)
print(enigma_on_text(rotor_1,rotor_4,rotor_3,'TCBDQZJHIGNLMKOPERSAVUWXYF',reflector,enigma_ciphertext,'NDU'))