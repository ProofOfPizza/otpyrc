from ciphertexts import vigenere_ciphertext as CIPHERTEXT
import reference as ref
import itertools as I
import numpy as NP
import re

CIPHER_LENGTH = 7
NUMBER_OF_RESULTS_CONSIDERED = 50
REFERENCE = ref.dutch


def do_permutations(cipherlength):
    list_of_numbers = range(1, cipherlength + 1)
    l = list(I.permutations(list_of_numbers))
    return l


def put_stop_in_text(ciphertext, cipherlength, permutation):  # doet - op plaats waar korte rijen zitten
    number_of_long_columns = len(ciphertext) % cipherlength
    number_of_short_columns = cipherlength - number_of_long_columns
    length_of_short_columns = int(len(ciphertext) / cipherlength)
    length_of_long_columns = length_of_short_columns + 1
    dict_with_distribution = dict()
    i = 0
    index = 0
    for element in range(number_of_long_columns):
        dict_with_distribution[element + 1] = 'I'
        i += 1
    for k in range(i, cipherlength):
        dict_with_distribution[k + 1] = '0'
    for element in permutation:
        for k, v in dict_with_distribution.items():
            if k == element:
                if v == 'I':
                    number_of_long_columns += 1
                    index += length_of_long_columns
                elif v == '0':
                    number_of_short_columns += 1
                    index += length_of_short_columns
                    ciphertext = ciphertext[:index] + '-' + ciphertext[index:]
                    index += 1
    # print(dict_with_distribution)
    a = [ciphertext[i:i + length_of_long_columns] for i in range(0, len(ciphertext), length_of_long_columns)]
    # print(a)
    new_list = []
    i = 0
    for element in permutation:
        new_list.append(a[element - 1])
        i += 1
    # print(new_list)
    return new_list

def make_text_of_list(list):
    length = len(list[0])
    i = 0
    new_text = ''
    while i < length:
        for element in list:
            if element[i] != '-':
                new_text += element[i]
        i += 1
    # print(new_text)
    return new_text

def faculty(number):
    product = 1
    for element in range(1, number + 1):
        product *= element
    return product


def make_all_texts(ciphertext, cipherlength):
    permutations = do_permutations(cipherlength)
    number_of_results = faculty(cipherlength)
    result = NP.zeros(number_of_results, dtype='O')
    index = 0
    for p in permutations:
        x1 = put_stop_in_text(ciphertext, cipherlength, p)
        x2 = make_text_of_list(x1)
        result[index] = x2
        print(index)
        index += 1
    return result


def find_trios_who_appear_often(texts):
    list_with_results=[]
    delers_5 = 0
    delers_6 = 0
    delers_7 = 0
    delers_8 = 0
    delers_9 = 0
    delers_10 = 0
    new_list=[]
    index=0
    for t in texts:
        for i in range(5):
            a=t[i:i+3]
            b=[m.start() for m in re.finditer(a, t)]
            distances = [abs(x1-x2) for x1 in b for x2 in b if x1<x2]
            delers_5 += len([x for x in distances if x % 5])
            delers_6 += len([x for x in distances if x % 6])
            delers_7 += len([x for x in distances if x % 7])
            delers_8 += len([x for x in distances if x % 8])
            delers_9 += len([x for x in distances if x % 9])
            delers_10 += len([x for x in distances if x % 10])
        new_list.append([delers_5,delers_6,delers_7,delers_8,delers_9,delers_10])
        list_with_results.append([t,new_list])
        #print('lllllllllllll', list_with_results)
        delers_5 = 0
        delers_6 = 0
        delers_7 = 0
        delers_8 = 0
        delers_9 = 0
        delers_10 = 0
        new_list=[]
        if index % 10000 == 1:
            print(index)
        index+=1
    return list_with_results


def which_cipherlength_per_text(list_with_results):
    list_text_and_cipherlength=[]
    for element in list_with_results:
        text=element[0]
        list_with_divisors=element[1]
        index=0
        maximum=0
        for i in range(len(list_with_divisors)):
            if list_with_divisors[i]>maximum:
                maximum=list_with_divisors[i]
                index=i
        list_text_and_cipherlength.append([text,index+5])
    return list_text_and_cipherlength

def sort_max_to_min(frequency_list):
    for i in range(len(frequency_list) - 1):
        if frequency_list[i][1] < frequency_list[i + 1][1]:
            tmp = frequency_list[i]
            frequency_list[i] = frequency_list[i + 1]
            frequency_list[i + 1] = tmp
            sort_max_to_min(frequency_list)
    return frequency_list


def frequency_analysis(list_with_results):
    # maakt dict met frequencies op 0 van heel alfabet
    alfabet = 'abcdefghijklmnopqrstuvwxyz'
    index=0
    big_list_with_results = []
    for element in list_with_results:
        text = element[0]
        cipherlength = element[1]
        length_one_column = 0
        dict_with_results1 = dict()

        for element in alfabet:
            dict_with_results1[element] = 0

        for i in range(len(text)):
            if i % cipherlength == 0:
                dict_with_results1[text[i]] += 1
                length_one_column += 1
            i += 1

        result_list = []
        for k, v in dict_with_results1.iteritems():
            result_list.append((k, (v / float(length_one_column)) * 100))
        big_list_with_results.append([text, result_list])
        print(index)
        index+=1
    return big_list_with_results

def calculate_fitness(frequency_list, reference):  # Deze moeten beiden lijsten zijn met tuples!
    nieuwelijst = list()
    reference = sort_max_to_min(reference)
    frequency_list = sort_max_to_min(frequency_list)
    for i in range(len(reference)):
        nieuwelijst.append((reference[i][0], frequency_list[i][0], abs(reference[i][1] - frequency_list[i][1])))
        # nieuwelijst.append((reference[i][0], frequency_list[i][0], abs(reference[i][1] - frequency_list[i][1]) * (1+1) ))
    return nieuwelijst

def calculate_average(list_of_frequencies):
    sum = 0
    for i in range(len(list_of_frequencies)):
        sum += (list_of_frequencies[i][2]) ** 2
    average = sum / len(list_of_frequencies)
    return average


def get_best_results(ciphertext,cipherlength,reference):
    list_of_averages=[]
    texts=make_all_texts(ciphertext,cipherlength)
    texts_and_lengths=find_trios_who_appear_often(texts)
    texts_and_one_length=which_cipherlength_per_text(texts_and_lengths)
    frequency_lists=frequency_analysis(texts_and_one_length)
    index=0
    for f in frequency_lists:
        c=calculate_fitness(f[1],reference)
        average=calculate_average(c)
        list_of_averages.append([f[0],f[1],average,cipherlength])
        print(index)
        index+=1
    list_of_averages.sort(key=lambda x: x[2])
    best_results=list_of_averages[0:NUMBER_OF_RESULTS_CONSIDERED]
    return best_results

def frequency_analysis_per_column(best_results):
    alfabet = 'abcdefghijklmnopqrstuvwxyz'
    alfabet_beginning_with_e='efghijklmnopqrstuvwxyzabcd'
    code_word=''
    best_results_list=[]
    for b in best_results:
        text = b[0]
        cipher_length = b[3]
        for j in range(cipher_length):
            dict_with_results1 = dict()
            for element in alfabet:
                dict_with_results1[element] = 0
            for i in range(len(text)):
                if i%cipher_length==j:
                    dict_with_results1[text[i]] += 1
            maximum=0
            letter=''
            for k,v in dict_with_results1.items():
                if v>maximum:
                    letter=k
                    maximum=v
            index_in_e_alphabet=alfabet_beginning_with_e.find(letter)
            code_word+=alfabet[index_in_e_alphabet]
            #meest voorkomende letter komt overeen met e
        best_results_list.append([b[0],code_word])
        print(code_word)
        code_word=''
    return best_results_list

def get_dutch_texts(best_results_list):
    alfabet = 'abcdefghijklmnopqrstuvwxyz'
    texts=[]
    for b in best_results_list:
        new_text=''
        text=b[0]
        code_word=b[1]
        list_with_positions = []
        for i in range(len(code_word)):
            list_with_positions.append(alfabet.find(code_word[i]))
        for j in range(len(text)):
            index=(j+len(code_word))%len(code_word)
            index_letter_in_alphabet=alfabet.find(text[j])
            new_letter=alfabet[(index_letter_in_alphabet-index)%26]
            new_text+=new_letter
        texts.append(new_text)
    return texts

step_1=get_best_results(CIPHERTEXT,CIPHER_LENGTH,ref.dutch)
print(step_1)
step_2=frequency_analysis_per_column(step_1)
print(step_2)
step_3=get_dutch_texts(step_2)
print(step_3)
